﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KindergartenCountingGameProblem.Tests
{
    [TestFixture]
    public class WhenLookingAtASentence
    {
        private CountingGame _solver;

        [SetUp]
        public void Setup()
        {
            _solver = new CountingGame();
        }

        [Test]
        public void And_the_sentence_is_null_then_an_exception_is_null()
        {
            Assert.Throws<ArgumentNullException>(() => _solver.CountWords(null));
        }

        [Test]
        public void AndTheSentenceISAnEmptyStringThenZeroShouldBeReturned()
        {
            var result = _solver.CountWords("");

            Assert.AreEqual(0, result);
        }

        [Test]
        public void AndTheSentenceIsASingleWordThenOneShouldBeReturned()
        {
            var result = _solver.CountWords("cat");

            Assert.AreEqual(1, result);
        }

        [Test]
        public void And_the_sentence_is_a_period_then_0_is_returned()
        {
            var result = _solver.CountWords(".");

            Assert.AreEqual(0, result);
        }

        [Test]
        public void And_the_sentence_is_a_cat_in_the_hat_then_4_is_returned()
        {
            var result = _solver.CountWords("cat in the hat");

            Assert.AreEqual(4, result);
        }

        [Test]
        public void And_the_sentence_contains_9_words_with_random_periods_then_9_is_returned()
        {
            var result = _solver.CountWords("Shsssssssssh ... I am hunting wabbits. Heh Heh Heh Heh ...");

            Assert.AreEqual(9, result);
        }


        [Test]
        public void And_the_sentence_contains_10_words_with_exclamation_points_and_periods_then_10_is_returned()
        {
            var result = _solver.CountWords("I did! I did! I did taw a putty tat.");

            Assert.AreEqual(10, result);
        }

        [Test]
        public void And_the_sentence_is_Meep_Meep_with_an_exclamation_point_then_2_is_returned()
        {
            var result = _solver.CountWords("Meep Meep!");

            Assert.AreEqual(2, result);
        }

        [Test]
        public void And_the_sentence_is_7_words_and_a_period_then_7_is_returned()
        {
            var result = _solver.CountWords("I tot I taw a putty tat.");

            Assert.AreEqual(7, result);
        }

        [Test]
        public void AndTheSentenceIsSpecialCharactersSeparatedBySpacesThen0IsReturned()
        {
            var result = _solver.CountWords("!@# $%^ ^&&()(");

            Assert.AreEqual(0, result);
        }

        

    }


}
