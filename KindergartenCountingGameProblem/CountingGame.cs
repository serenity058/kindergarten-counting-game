﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KindergartenCountingGameProblem
{ // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class CountingGame
    {
        public int CountWords(string sentence)
        {
            var count = 0;
            char character;
            string word = "";

            if (sentence == null)
            {
                throw new ArgumentNullException("sentence");
            }

            for (int i = 0; i < sentence.Length; i++)
            {
                character = sentence[i];
                if (Char.IsLetter(character))
                {
                    //it is not a letter then we add it to the word
                    word += character;
                }
                else
                {
                    //it is not a letter so we need to check if word has a length greater than 0
                    if (word.Length > 0)
                    {
                        //then we have found a word
                        count++;
                    }
                    word = "";
                }
            }
            if (word.Length > 0)
            {
                count++;
            }
            return count;
        }

        private bool FilterThing(char arg, int index)
        {
            throw new NotImplementedException();
        }
    }
}
